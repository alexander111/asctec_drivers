cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME asctec_autopilot)
project(${PROJECT_NAME})

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)



find_package(catkin REQUIRED
                COMPONENTS roscpp nodelet geometry_msgs sensor_msgs nav_msgs asctec_msgs diagnostic_updater)


catkin_package(
        INCLUDE_DIRS include
        LIBRARIES autopilot autopilot_nodelet
        CATKIN_DEPENDS roscpp nodelet geometry_msgs sensor_msgs nav_msgs asctec_msgs diagnostic_updater
  )


include_directories(include include/asctec_autopilot)
include_directories(${catkin_INCLUDE_DIRS})



#include files
LIST(APPEND INCLUDEFILES
    include/asctec_autopilot/autopilot.h
    include/asctec_autopilot/autopilot_nodelet.h
    include/asctec_autopilot/crc16.h
    include/asctec_autopilot/serialinterface.h
    include/asctec_autopilot/telemetry.h
)




# create autopilot library
add_library(autopilot src/autopilot.cpp
                                src/serial_interface.cpp
                                src/crc16.cpp 
                                src/telemetry.cpp)
add_dependencies(autopilot ${catkin_EXPORTED_TARGETS})
target_link_libraries(autopilot ${catkin_LIBRARIES})


# create autopilot_nodelet library
add_library(autopilot_nodelet src/autopilot_nodelet.cpp)
add_dependencies(autopilot_nodelet ${catkin_EXPORTED_TARGETS})
target_link_libraries(autopilot_nodelet autopilot)
target_link_libraries(autopilot_nodelet ${catkin_LIBRARIES})


# create autopilot_node executable
add_executable(autopilot_node src/autopilot_node.cpp)
add_dependencies(autopilot_node ${catkin_EXPORTED_TARGETS})
target_link_libraries(autopilot_node autopilot)
target_link_libraries(autopilot_node ${catkin_LIBRARIES})

